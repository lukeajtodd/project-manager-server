## Postgresql Setup

- `brew update`
- `brew install postgresql`
- `initdb /your/location/postgres` e.g. `initdb /usr/local/var/postgres`

- `createdb mydatabasename`

Starting and stopping the database server

```
pg_ctl -D /usr/local/var/postgres start
pg_ctl -D /usr/local/var/postgres stop
```

Connecting to the DB
`psql` or `psql databasename`